// liens utiles :
// https://discord.js.org/#/docs/main/stable/general/welcome
// https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/

// connexion du bot au serveur
var Discord = require('discord.js');
const bot = new Discord.Client();
const client = new Discord.Client();
var prefix = "!"

// insérer le bon token dans les " "
bot.login("NjEwMzc3NzkxMTQxMzE0NTYy.XVEwww.bYF7deignWDBZ1W6z1n9e5fpd60");
client.login("NjEwMzc3NzkxMTQxMzE0NTYy.XVEwww.bYF7deignWDBZ1W6z1n9e5fpd60");

// retourne une phrase au hasard issue de la liste n
function random_phrase(n) {
    var minNumber = 1; // le minimum
    var maxNumber = 7; // le maximum
    var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
    var phrase = n[randomnumber]
    return phrase;
}

// Message lors de l'arriver sur le serveur
client.on('guildMemberAdd', user => {
    // get obtenu en tappant " \#nomduchannel " sur discord. 
    user.guild.channels.get("610374355171672068").send("Bienvenue sur le serveur ! :yum: ");
})

// définition des réponses du bot en fonction de la commande entrée
bot.on("message", function (msg) {
    if (msg.content === prefix + "liste") {
        msg.reply(liste_commande);
    }
    if (msg.content === prefix + "ausecours") {
        msg.reply(random_phrase(ausecours))
    };
    if (msg.content === prefix + "need love") {
        msg.reply(random_phrase(love))
    };
    if (msg.content === prefix + "need motivation") {
        msg.reply(random_phrase(motivation))
    };
    if (msg.content === prefix + "need help") {
        msg.reply(random_phrase(help))
    };
});

const liste_commande = "liste des commande du bot : !ausecours, !need love, !need motivation, !need help.";

const love = {
    1: "Tu es le meilleur !",
    2: "Je t'aime",
    3: "T'es nul.le mais je t'aime quand même :kissing_closed_eyes: ",
    4: "Demande un câlin à Boris ou à Emma",
    5: "Fais-toi une tappe énergique sur l'épaule",
    6: "Je t'aime vraiment très fort et plus encore",
    7: "Tu es beau/belle"
};
const motivation = {
    1: "Tu y es presque",
    2: "Du gras, du sel, du sucre",
    3: "Écoute ta chanson préféree",
    4: "Je te comprends",
    5: "Respire, et si ça suffit pas, re-respire",
    6: "La force est avec toi",
    7: "Tu n'as jamais été aussi proche de l'arrivée"
};
const help = {
    1: "Écoute ton coeur",
    2: "Va fumer si tu es fumeur, sinon, café.",
    3: "Pousse un coup, ça ira mieux",
    4: "Google est ton ami",
    5: "Demande à ton voisin",
    6: "En vrai moi ça va !",
    7: "Prends le temps de te poser et avance à ton rythme"
};
const ausecours = {
    1: "Pas de corde pour te tirer d'affaire, pas de bouée pour éviter que tu boive la tasse, mais deux grandes oreilles pour écouter tes problèmes :grin: ",
    2: "Demain ça ira mieux",
    3: "Réjouis-toi, tu aurais pu être un manchot !",
    4: "Toi-même tu sais :grin: ",
    5: "R.T.F.M",
    6: "Fais des trucs",
    7: "Ferme les yeux et ouvre tes chakras"
};

// listage des commandes à l'arrivé sur le serveur
// isérer ici !

// toutes les 60s affiche un message
// bot.on("ready", function(){
//     message.channel.send("TEST auto MsG");
//         var interval = setInterval(function () {
//         message.channel.send("123")
//     }, 30 * 1000);
// })
// "Pour avoir la liste complète des commandes, tappe !liste"